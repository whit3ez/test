import sys
def printVal(distn):
    for i in range(n):
        print(distn[i])


def minDistance(distn, sptSet):
    min = sys.maxsize
    for v in range(n):
        if distn[v] < min and sptSet[v] == False:
            min = distn[v]
            min_index = v
    return min_index


def dijkstra(src, graph):
    distn = [sys.maxsize] * n
    distn[src] = 0
    sptSet = [False] * n

    for cout in range(n):
        u = minDistance(distn, sptSet)
        sptSet[u] = True
        for v in range(n):
            if graph[u][v] > 0 and sptSet[v] == False and distn[v] > distn[u] + graph[u][v]:
                distn[v] = distn[u] + graph[u][v]
    printVal(distn)

def bellman_ford(graph, source):
    n, inf = len(graph), float('inf')
    v = range(n)
    dist = [inf for _ in v]
    dist[source] = 0
    for _ in range(n - 1):
        for i in v:
            for j in v:
                w = graph[i][j]
                if dist[i] != inf and dist[i] + w < dist[j]:
                    dist[j] = dist[i] + w

    for i in v:
        for j in v:
            w = graph[i][j]
            if dist[i] != inf and dist[i] + w < dist[j]:
                print("отрицательные элементы")

    return dist

def floydWarshall(graph):
  for k in range(n):
    for i in range(n):
      for j in range(n):
        #graph[i][j] = graph[i][j] or (graph[i][k] and graph[k][j])
        graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j])


n = int(input())